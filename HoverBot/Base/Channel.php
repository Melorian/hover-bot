<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 13:34
 */

namespace HoverBot\Base;

use HoverBot\Base\Components\ChannelPurpose;
use HoverBot\Base\Components\ChannelTopic;
use HoverBot\Exceptions\DataException;
use HoverBot\Exceptions\Exception;

/**
 * Канал
 *
 * Class Channel
 * @package HoverBot\Base
 */
class Channel
{
    /** @var Context Текущий контекст подключения */
    protected $context;

    /** @var string Идентификатор */
    protected $id;

    /** @var string Название */
    protected $name;

    /** @var boolean Канал является каналом ಠ_ಠ */
    protected $is_channel = true;

    /** @var integer Дата создания */
    protected $created;

    /** @var User Создатель */
    protected $creator;

    /** @var boolean Канал в архиве */
    protected $is_archived;

    /** @var boolean Канал является главным */
    protected $is_general;

    /** @var User[] Участники */
    protected $members = [];

    /** @var ChannelTopic Тема канала */
    protected $topic;

    /** @var ChannelPurpose Причина создания канала */
    protected $purpose;

    /** @var boolean Текущий пользователь входит в канал */
    protected $is_member;

    /** @var string Дата последнего прочтения */
    protected $last_read;

    /** @var Event Последнее сообщение в чате */
    protected $latest;

    /** @var integer Количество непрочтенных сообщений всего */
    protected $unread_count;

    /** @var integer Количество непрочтенных сообщений для пользователя */
    protected $unread_count_display;

    /**
     * @param Context $context
     * @param array $data
     * @throws DataException
     */
    public function __construct(Context &$context, array $data)
    {
        $this->context = $context;

        if (isset($data['topic']))
            $this->setTopic(new ChannelTopic($this, $data['topic']));
        else
            $this->setTopic(new ChannelTopic($this, []));

        if (isset($data['purpose']))
            $this->setPurpose(new ChannelPurpose($this, $data['purpose']));
        else
            $this->setPurpose(new ChannelPurpose($this, []));

        $this->setId(isset($data['id']) ? $data['id'] : null);
        $this->setName(isset($data['name']) ? $data['name'] : null);
        $this->setIsChannel(isset($data['is_channel']) ? $data['is_channel'] : null);
        $this->setCreated(isset($data['created']) ? $data['created'] : null);
        $this->setCreator(isset($data['creator']) ? $context->getUser($data['creator']) : null);
        $this->setIsArchived(isset($data['is_archived']) ? $data['is_archived'] : null);
        $this->setIsGeneral(isset($data['is_general']) ? $data['is_general'] : null);
        $this->setIsMember(isset($data['is_member']) ? $data['is_member'] : null);
        $this->setLastRead(isset($data['last_read']) ? $data['last_read'] : null);
        $this->setUnreadCount(isset($data['unread_count']) ? $data['unread_count'] : null);
        $this->setUnreadCountDisplay(isset($data['unread_count_display']) ? $data['unread_count_display'] : null);

        try
        {
            $this->setLatest(isset($data['latest']) ? Event::createFromArray($data['latest']) : null);
        }
        catch (Exception $e)
        {
            $context->getBot()->log(vsprintf("\t\tНе удалось получить последнее сообщение в канале %s: %s", [$this->getId(), $e->getMessage()]));
        }

        if (! empty($data['members']))
            foreach ($data['members'] as $member)
                $this->addMember($context->getUser($member));
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Channel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Channel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsChannel()
    {
        return $this->is_channel;
    }

    /**
     * @param boolean $is_channel
     *
     * @return Channel
     */
    public function setIsChannel($is_channel)
    {
        $this->is_channel = !$is_channel;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     *
     * @return Channel
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     *
     * @return Channel
     */
    public function setCreator(User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsArchived()
    {
        return $this->is_archived;
    }

    /**
     * @param boolean $is_archived
     *
     * @return Channel
     */
    public function setIsArchived($is_archived)
    {
        $this->is_archived = $is_archived;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsGeneral()
    {
        return $this->is_general;
    }

    /**
     * @param boolean $is_general
     *
     * @return Channel
     */
    public function setIsGeneral($is_general)
    {
        $this->is_general = $is_general;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param User[] $members
     *
     * @return Channel
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * Добавляет нового члена канала
     *
     * @param User $user
     * @param boolean|true $replaceIfExists Заменить при совпадении
     *
     * @return Channel
     * @throws DataException
     */
    public function addMember (User &$user, $replaceIfExists = true)
    {
        if (array_key_exists($user->getId(), $this->members) AND !$replaceIfExists)
            throw new DataException(vsprintf('Пользователь с ID = %s уже добавлен в канал', $user->getId()));

        $this->members[$user->getId()] = $user;

        return $this;
    }

    /**
     * Берет члена канала по ID
     *
     * @param $id
     *
     * @return User
     * @throws DataException
     */
    public function getMember ($id)
    {
        if (! array_key_exists($id, $this->members))
            throw new DataException(vsprintf('Пользователь с ID = %s не является членом канала', $id));

        return $this->members[$id];
    }

    /**
     * @return ChannelTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param ChannelTopic $topic
     *
     * @return Channel
     */
    public function setTopic(ChannelTopic &$topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * @return ChannelPurpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param ChannelPurpose $purpose
     *
     * @return Channel
     */
    public function setPurpose(ChannelPurpose &$purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsMember()
    {
        return $this->is_member;
    }

    /**
     * @param boolean $is_member
     *
     * @return Channel
     */
    public function setIsMember($is_member)
    {
        $this->is_member = $is_member;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastRead()
    {
        return $this->last_read;
    }

    /**
     * @param string $last_read
     *
     * @return Channel
     */
    public function setLastRead($last_read)
    {
        $this->last_read = $last_read;

        return $this;
    }

    /**
     * @return Event
     */
    public function getLatest()
    {
        return $this->latest;
    }

    /**
     * @param Event $latest
     *
     * @return Channel
     */
    public function setLatest($latest)
    {
        $this->latest = $latest;

        return $this;
    }

    /**
     * @return int
     */
    public function getUnreadCount()
    {
        return $this->unread_count;
    }

    /**
     * @param int $unread_count
     *
     * @return Channel
     */
    public function setUnreadCount($unread_count)
    {
        $this->unread_count = $unread_count;

        return $this;
    }

    /**
     * @return int
     */
    public function getUnreadCountDisplay()
    {
        return $this->unread_count_display;
    }

    /**
     * @param int $unread_count_display
     *
     * @return Channel
     */
    public function setUnreadCountDisplay($unread_count_display)
    {
        $this->unread_count_display = $unread_count_display;

        return $this;
    }



}