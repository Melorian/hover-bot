<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 11:43
 */

namespace HoverBot\Base;

use HoverBot\Base\Components\ContextSelf;
use HoverBot\Base\Components\ContextTeam;
use HoverBot\Bot;
use HoverBot\Exceptions\DataException;
use HoverBot\Exceptions\Exception;

/**
 * Текущий контекст подключения
 *
 * Class Context
 * @package HoverBot\Base
 */
class Context
{
    /** @var Bot Бот */
    protected $bot;

    /** @var string Авторизованный URL */
    protected $url;

    /** @var ContextSelf Подключенный пользователь */
    protected $self;

    /** @var ContextTeam Команда */
    protected $team;

    /** @var User[] Пользователи команды */
    protected $users = [];

    /** @var Channel[] Публичные каналы команды */
    protected $channels = [];

    /** @var Group[] Приватные группы */
    protected $groups = [];

    /** @var Im[] Приватные диалоги */
    protected $ims = [];

    /** @var Bot[] Боты */
    protected $bots = [];

    /**
     * @param Bot $bot
     * @param array $data
     * @throws DataException
     */
    public function __construct(Bot &$bot, array $data)
    {
        $bot->log("Создаю контекст...");

        $this->setBot($bot);

        if (empty($data['url']))
            throw new DataException('Отсутствует авторизованный URL в контексте');
        else
            $this->setUrl($data['url']);

        $bot->log("\tЗагружаю пользователей...");

        if (isset($data['users']))
            foreach ($data['users'] as $user)
                $this->addUser(new User($this, $user));

        $bot->log("\tЗагружаю каналы...");

        if (isset($data['channels']))
            foreach ($data['channels'] as $channel)
                $this->addChannel(new Channel($this, $channel));

        $bot->log("\tЗагружаю группы...");

        if (isset($data['groups']))
            foreach ($data['groups'] as $group)
                $this->addGroup(new Group($this, $group));

        $bot->log("\tЗагружаю диалоги...");

        if (isset($data['ims']))
            foreach ($data['ims'] as $im)
                $this->addIm(new Im($this, $im));

        $bot->log("\tОпределяю свое место в мире...");

        if (empty($data['self']))
            throw new DataException('Отсутствует информация о пользователе контекста');
        else
            $this->setSelf(new ContextSelf($this, $data['self']));

        $bot->log("\tУзнаю о мире, в котором нахожусь...");

        if (empty($data['team']))
            throw new DataException('Отсутствует информация о команде');
        else
            $this->setTeam(new ContextTeam($this, $data['team']));

        $bot->log("Контекст загружен!");
    }

    /**
     * @return Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param Bot $bot
     *
     * @return Context
     */
    protected function setBot($bot)
    {
        $this->bot = $bot;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Context
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return ContextSelf
     */
    public function getSelf()
    {
        return $this->self;
    }

    /**
     * @param ContextSelf $self
     *
     * @return Context
     */
    public function setSelf(ContextSelf &$self)
    {
        $this->self = $self;

        return $this;
    }

    /**
     * @return ContextTeam
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param ContextTeam $team
     *
     * @return Context
     */
    public function setTeam(ContextTeam &$team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User[] $users
     *
     * @return Context
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Добавляет нового пользователя
     *
     * @param User $user
     * @param boolean|true $replaceIfExists Заменить при совпадении
     *
     * @return Context
     * @throws DataException
     */
    public function addUser (User &$user, $replaceIfExists = true)
    {
        if (array_key_exists($user->getId(), $this->users) AND !$replaceIfExists)
            throw new DataException(vsprintf('Пользователь с ID = %s уже добавлен', $user->getId()));

        $this->users[$user->getId()] = $user;

        return $this;
    }

    /**
     * Берет пользователя по ID
     *
     * @param $id
     *
     * @return User
     * @throws DataException
     */
    public function getUser ($id)
    {
        if (! array_key_exists($id, $this->users))
            throw new DataException(vsprintf('Пользователь с ID = %s не существует', $id));

        return $this->users[$id];
    }

    /**
     * Проверяет существование пользователя
     *
     * @param string $userId
     *
     * @return boolean
     */
    public function hasUser ($userId)
    {
        return array_key_exists($userId, $this->users);
    }

    /**
     * @return Channel[]
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @param Channel[] $channels
     *
     * @return Context
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;

        return $this;
    }

    /**
     * Добавляет новый канал
     *
     * @param Channel $channel
     * @param boolean|true $replaceIfExists Заменить при совпадении
     *
     * @return Context
     * @throws DataException
     */
    public function addChannel (Channel &$channel, $replaceIfExists = true)
    {
        if (array_key_exists($channel->getId(), $this->channels) AND !$replaceIfExists)
            throw new DataException(vsprintf('Канал с ID = %s уже добавлен', $channel->getId()));

        $this->channels[$channel->getId()] = $channel;

        return $this;
    }

    /**
     * Берет канал по ID
     *
     * @param $id
     *
     * @return Group
     * @throws DataException
     */
    public function getChannel ($id)
    {
        if (! array_key_exists($id, $this->channels))
            throw new DataException(vsprintf('Канал с ID = %s не существует', $id));

        return $this->channels[$id];
    }

    /**
     * Проверяет существование канала
     *
     * @param string $channelId
     *
     * @return boolean
     */
    public function hasChannel ($channelId)
    {
        return array_key_exists($channelId, $this->channels);
    }

    /**
     * @return Group[]
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param Group[] $groups
     *
     * @return Context
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * Добавляет новую группу
     *
     * @param Group $group
     * @param boolean|true $replaceIfExists Заменить при совпадении
     *
     * @return Context
     * @throws DataException
     */
    public function addGroup (Group &$group, $replaceIfExists = true)
    {
        if (array_key_exists($group->getId(), $this->groups) AND !$replaceIfExists)
            throw new DataException(vsprintf('Группа с ID = %s уже добавлена', $group->getId()));

        $this->groups[$group->getId()] = $group;

        return $this;
    }

    /**
     * Берет группу по ID
     *
     * @param $id
     *
     * @return Group
     * @throws DataException
     */
    public function getGroup ($id)
    {
        if (! array_key_exists($id, $this->groups))
            throw new DataException(vsprintf('Группа с ID = %s не существует', $id));

        return $this->groups[$id];
    }

    /**
     * Проверяет существование группы
     *
     * @param string $groupId
     *
     * @return boolean
     */
    public function hasGroup ($groupId)
    {
        return array_key_exists($groupId, $this->channels);
    }

    /**
     * @return IM[]
     */
    public function getIms()
    {
        return $this->ims;
    }

    /**
     * @param IM[] $ims
     *
     * @return Context
     */
    public function setIms($ims)
    {
        $this->ims = $ims;

        return $this;
    }

    /**
     * Добавляет новый диалог
     *
     * @param Im $im
     * @param boolean|true $replaceIfExists Заменить при совпадении
     *
     * @return Context
     * @throws DataException
     */
    public function addIm (Im &$im, $replaceIfExists = true)
    {
        if (array_key_exists($im->getId(), $this->ims) AND !$replaceIfExists)
            throw new DataException(vsprintf('Группа с ID = %s уже добавлена', $im->getId()));

        $this->ims[$im->getId()] = $im;

        return $this;
    }

    /**
     * Берет диалог по ID
     *
     * @param $id
     *
     * @return Im
     * @throws DataException
     */
    public function getIm ($id)
    {
        if (! array_key_exists($id, $this->ims))
            throw new DataException(vsprintf('Диалог с ID = %s не существует', $id));

        return $this->ims[$id];
    }

    /**
     * Проверяет существование диалога
     *
     * @param string $imId
     *
     * @return boolean
     */
    public function hasIm ($imId)
    {
        return array_key_exists($imId, $this->ims);
    }

    /**
     * @return Bot[]
     */
    public function getBots()
    {
        return $this->bots;
    }

    /**
     * @param Bot[] $bots
     *
     * @return Context
     */
    public function setBots($bots)
    {
        $this->bots = $bots;

        return $this;
    }

    public function findChannelById ($id)
    {
        $letter = mb_strtoupper(mb_substr($id, 0, 1));

        try
        {
            switch ($letter)
            {
                case 'C':
                    return $this->hasChannel($id) ? $this->getChannel($id) : null;

                case 'G':
                    return $this->hasGroup($id) ? $this->getGroup($id) : null;

                case 'D':
                    return $this->hasIm($id) ? $this->getIm($id) : null;

                default:
                    return null;
            }
        }
        catch (Exception $e)
        {
            return null;
        }
    }

}