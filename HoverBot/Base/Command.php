<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 30.10.15
 * Time: 16:14
 */

namespace HoverBot\Base;

abstract class Command
{
    /**
     * Бот
     *
     * @return \HoverBot\Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param \HoverBot\Bot $bot
     */
    protected function setBot($bot)
    {
        $this->bot = $bot;
    }

    /** @const string Временное решение для поиска команд в папках */
    const TEMPLATE = 'Command.php';

    /** @var \HoverBot\Bot */
    protected $bot;

    /** @var string Тип сообщения, к которому применима команда */
    protected $allowedEventType;

    /** @var \HoverBot\Base\Event Текущее событие сервера */
    protected $event;

    /**
     * Ищет название команды по названию файла
     *
     * @param $fileName
     * @return null|string
     */
    public static function scanCommandNameFromFile ($fileName)
    {
        $templatePos = mb_strripos($fileName, static::TEMPLATE);
        $templateLen = mb_strlen(static::TEMPLATE);
        $fileNameLen = mb_strlen($fileName);
        $commandLen = $fileNameLen - $templateLen;

        if (($templatePos !== false) AND ($templatePos == $commandLen))
            return mb_substr($fileName, 0, $commandLen);

        return null;
    }

    /**
     * @param $bot
     */
    public function __construct ($bot)
    {
        $this->setBot($bot);

        $this->initialize();
    }

    /**
     * @return void
     */
    abstract function initialize ();

    /**
     * @param $event
     *
     * @return $this
     */
    public function setEvent ($event)
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return Event
     */
    public function getEvent ()
    {
        return $this->event;
    }

    /**
     * Полная проверка, может ли команда обработать сообщение
     *
     * @param $event
     *
     * @return bool
     */
    public function canHandleEvent ($event)
    {
        return (get_class($event) == $this->allowedEventType) AND $this->isApplicable($event);
    }

    /**
     * Внутренняя проверка команды, применима ли она к текущему сообщению
     *
     * @param Event $event
     * @return boolean
     */
    abstract function isApplicable ($event);

    /**
     * Выполнение команды
     *
     * @return void
     */
    abstract function execute ();

    public function process ($event)
    {
        $this->setEvent($event);

        $this->beforeExecute();
        $this->execute();
        $this->afterExecute();
    }

    public function beforeExecute ()
    {

    }

    public function afterExecute ()
    {

    }
}