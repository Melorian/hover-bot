<?php

namespace HoverBot\Base;

use GetOptionKit\Option;
use GetOptionKit\OptionCollection;
use GetOptionKit\OptionParser;
use HoverBot\Events\MessageEvent;
use HoverBot\Exceptions\CommandException;

/**
 * Class SimpleCommand
 * @package HoverBot\Base
 * @method MessageEvent getEvent()
 */
abstract class SimpleCommand extends Command
{
    const OPTION_REQUIRED_NONE      = 1;
    const OPTION_REQUIRED_ONCE      = 2;
    const OPTION_REQUIRED_MULTIPLE  = 3;

    const COMMAND_MODE_PLAIN    = 1;
    const COMMAND_MODE_CALL     = 2;
    const COMMAND_MODE_SLASH    = 3;

    protected $command;

    protected $allowedEventType = MessageEvent::class;

    /** @var string Способ использования команды */
    protected $commandMode = SimpleCommand::COMMAND_MODE_CALL;

    /** @var array Схема получения атрибутов запроса */
    protected $argumentsSchema = [];

    /** @var OptionParser Парсер запроса */
    protected $argumentsParser;

    /** @var string[] Разобранный список аргументов запроса */
    protected $requestArguments = [];

    /** @var string[] Разобранный список атрибутов запроса */
    protected $requestOptions = [];

    /**
     * Возвращает название команды
     *
     * @return string
     */
    abstract protected function command ();

    public function initialize ()
    {
        $command = mb_strtolower(trim((string)$this->command()));

        if (! $command)
            throw new CommandException(vsprintf('Необходимо указать текст активации для команды %s', [get_called_class()]));

        $this->command = $command;

        $options = new OptionCollection();
        $aliases = [];

        if (empty($this->argumentsSchema) OR !is_array($this->argumentsSchema))
            $this->argumentsSchema = [];

        foreach ($this->argumentsSchema as $argumentName => $argumentInfo)
        {
            $option = new Option();

            $option->long = mb_strtolower($argumentName);

            if (! empty($argumentInfo['alias']))
            {
                $alias = mb_strtolower($argumentInfo['alias']);

                if (mb_strlen($alias) != 1) // @todo: Пока по тупому, потом обрабатывать будем
                    continue;

                if (in_array($alias, $aliases))
                    continue;

                $option->short = $alias;
            }

            if (empty($argumentInfo['boolean']))
            {
                switch ($argumentInfo['required'])
                {
                    default:
                    case static::OPTION_REQUIRED_NONE:
                        $option->optional();
                        break;

                    case static::OPTION_REQUIRED_ONCE:
                        $option->required();
                        break;

                    case static::OPTION_REQUIRED_MULTIPLE:
                        $option->multiple();
                        break;

                    default:
                        continue;
                }
            }
            else
                $option->flag();

            if (! empty($argumentInfo['description']))
                $option->desc($argumentInfo['description']);

            $options->add($option);
        }

        $this->argumentsSchema = $options;
        $this->argumentsParser = new OptionParser($this->argumentsSchema);
    }

    /**
     * @param MessageEvent $event
     * @return bool
     */
    public function isApplicable ($event)
    {
        if ($event->getSubtype())
            return false;

        $request = $this->parseArguments($event->getText());

        $this->requestOptions   = $request->toArray();
        $this->requestArguments = $request->getArguments();

        $firstArgument  = mb_strtolower(@$this->requestArguments[0] ?: null);
        $secondArgument = mb_strtolower(@$this->requestArguments[1] ?: null);

        switch ($this->commandMode)
        {
            default:
                return false;

            case static::COMMAND_MODE_PLAIN:
                return $firstArgument == $this->command;

            case static::COMMAND_MODE_SLASH:
                return '/'.$firstArgument == $this->command;

            case static::COMMAND_MODE_CALL:
                $selfId     = $this->getBot()->getContext()->getSelf()->getId();
                $selfName   = mb_strtolower('<@' . $selfId . '>');
                return ($firstArgument == $selfName OR $firstArgument == $selfName.':') AND ($secondArgument == $this->command);
        }

    }

    public function getArguments ($text)
    {
        preg_match_all('#(?<!\\\\)("|\')(?:[^\\\\]|\\\\.)*?\1|\S+#s', $text, $matches);

        if (! is_array($matches))
            $matches = [];
        else
            $matches = array_shift($matches);

        return $matches;
    }

    public function parseArguments ($text)
    {
        try
        {
            $arguments = $this->getArguments($text);
            $result = $this->argumentsParser->parse($arguments);

            return $result;
        }
        catch (\Exception $e)
        {
            $this->getBot()->log(vsprintf('Не удалось распознать строку: %s', [$e->getMessage()]));
            return [];
        }
    }
}