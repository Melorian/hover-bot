<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 06.11.15
 * Time: 15:37
 */

namespace HoverBot\Base;

/**
 * Реакция к контенту
 *
 * Class Reaction
 * @package HoverBot\Base
 */
class Reaction
{
    /** @var string Название реакции */
    protected $name;

    /** @var integer Количество использований реакций */
    protected $count;

    /** @var string[] Список пользователей, поставивших реакцию */
    protected $users;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setName(isset($data['name']) ? $data['name'] : null);
        $this->setCount(isset($data['count']) ? $data['count'] : null);
        $this->setUsers(isset($data['users']) ? $data['users'] : null);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Reaction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return Reaction
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param string[] $users
     *
     * @return Reaction
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Добавить реакцию пользователя
     *
     * @param string $userId
     *
     * @return Reaction
     */
    public function addUser ($userId)
    {
        $users = $this->getUsers();

        if (in_array($userId, $users))
            return $this;

        $users[] = $userId;

        $this->setUsers($users);

        $this->setCount($this->getCount()+1);

        return $this;
    }

}