<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 06.11.15
 * Time: 15:17
 */

namespace HoverBot\Base;

/**
 * Загруженный файл
 *
 * Class File
 * @package HoverBot\Base
 */
class File
{
    const MODE_HOSTED   = 'hosted';
    const MODE_EXTERNAL = 'external';
    const MODE_SNIPPET  = 'snippet';
    const MODE_POST     = 'post';

    /** @var string Идентификатор */
    protected $id;

    /** @var integer Дата создания */
    protected $created;

    /** @deprecated @var integer Дата создания */
    protected $timestamp;

    /** @var string Название самого файла */
    protected $name;

    /** @var string Указанное название */
    protected $title;

    /** @var string MIME-тип */
    protected $mimetype;

    /** @var string Тип */
    protected $filetype;

    /** @var string "Человеческий" тип */
    protected $pretty_type;

    /** @var User Автор */
    protected $user;

    /** @var string Тип загрузки (hosted, external, snippet, post) */
    protected $mode;

    /** @var boolean Возможно редактировать */
    protected $editable;

    /** @var boolean Находится ли сам файл на внешнем сервере */
    protected $is_external;

    /** @var boolean Тип внешнего сервера (например, dropbox, gdoc) */
    protected $external_type;

    /** @var integer Размер файла */
    protected $size;

    /** @var string Ссылка на содержимое внешнего файла */
    protected $url;

    /** @var string Ссылка на внешний файл для загрузки */
    protected $url_download;

    /** @var string Авторизованная ссылка на содержимое внешнего файла */
    protected $url_private;

    /** @var string Авторизованная ссылка для за загрузки внешнего файла */
    protected $url_private_download;

    /** @var string Предварительная картинка 64х64 */
    protected $thumb_64;

    /** @var string Предварительная картинка 80х80 */
    protected $thumb_80;

    /** @var string Предварительная картинка 360х360 */
    protected $thumb_360;

    /** @var string Предварительная анимированния гифка 360х360 */
    protected $thumb_360_gif;

    /** @var integer Ширина для предварительной картинки */
    protected $thumb_360_w;

    /** @var integer Высота для предварительной картинки */
    protected $thumb_360_h;

    /** @var string Ссылка на страницу файла в Слаке с подробной информацией */
    protected $permalink;

    /** @var string Страница редактирования файла (только для post и snippet) */
    protected $edit_link;

    /** @var string Превью текста файла */
    protected $preview;

    /** @var string Превью текста файла с улучшенным форматированием */
    protected $preview_highlight;

    /** @var integer Количество строк в превью */
    protected $lines;

    /** @var integer Количество строк еще (вне превью) */
    protected $lines_more;

    /** @var boolean Является ли файл публичным */
    protected $is_public;

    /** @var boolean Имеет ли файл ссылку для шаринга */
    protected $public_url_shared;

    /** @var string[] Список каналов, для которых расшарен файл */
    protected $channels;

    /** @var string[] Список групп, для которых расшарен файл */
    protected $groups;

    /** @var string[] Список диалогов, для которых расшарен файл */
    protected $ims;

    /** @var string Изначальный комментарий при загрузке файла */
    protected $initial_comment;

    /** @var integer Количество добавлений в избранное */
    protected $num_stars;

    /** @var boolean Добавлен ли файл в избранное у текущего пользователя */
    protected $is_starred;

    /** @var string[] Список каналов, к которым прикреплен файл */
    protected $pinned_to;

    /** @var Reaction[] Список реакций на файл */
    protected $reactions;

}