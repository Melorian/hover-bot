<?php

namespace HoverBot\Base;

use HoverBot\Events\MessageEvent;

abstract class RegexCommand extends Command
{
    protected $allowedEventType = MessageEvent::class;

    protected $regex;

    /**
     * @param MessageEvent $event
     * @return bool
     */
    public function isApplicable ($event)
    {
        return preg_match($this->regex, $event->getText()) === true;
    }
}