<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 30.10.15
 * Time: 18:10
 */

namespace HoverBot\Base;

use HoverBot\Bot;
use HoverBot\Exceptions\ApiException;

abstract class Event
{
    protected $bot;

    protected $raw;

    protected $date;

    protected $type;

    public function getType ()
    {
        return $this->type;
    }

    /**
     * Создает сообщение с нужными данными
     *
     * @param array $data
     *
     * @return static
     */
    public static function make (array $data)
    {
        return new static($data);
    }

    /**
     * Возвращает сущность сообщения из JSON
     *
     * @param string $data
     * @return \HoverBot\Base\Event
     * @throws ApiException
     */
    public static function createFromJson ($data)
    {
        $data = json_decode($data, true);

        return static::createFromArray($data);
    }

    /**
     * Возвращает сущность сообщения из массива
     *
     * @param array $data
     * @return \HoverBot\Base\Event
     * @throws ApiException
     */
    public static function createFromArray (array $data)
    {
        if (! is_array($data))
            throw new ApiException('Не удалось распознать сообщение сервера');

        if (empty($data['type']))
            throw new ApiException('Неизвестный тип сообщения сервера');

        $type       = $data['type'];
        $eventType  = str_replace(' ', '', mb_convert_case(str_replace('_', ' ', str_replace('-', '_', $type)), MB_CASE_TITLE, "UTF-8"));
        $eventClass = '\\HoverBot\\Events\\' . $eventType . 'Event';

        if (! class_exists($eventClass))
            throw new ApiException(vsprintf('Отсутствует обработчик сообщений сервера типа %s', $type));

        if (! is_subclass_of($eventClass, Event::class))
            throw new ApiException(vsprintf('Неверный обработчик сообщений сервера типа %s', $type));

        return $eventClass::make($data);
    }

    public function __construct ($data)
    {
        $this->raw  = $data;
        $this->date = time();

        $this->parseData();

        $this->callback();
    }

    abstract protected function parseData ();

    protected function callback ()
    {

    }

    /**
     * @return Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param Bot $bot
     *
     * @return Event
     */
    public function setBot(Bot &$bot)
    {
        $this->bot = $bot;

        return $this;
    }


}