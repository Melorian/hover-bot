<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 18:13
 */

namespace HoverBot\Base;

use HoverBot\Base\Components\MessageAttachment;

/**
 * Исходящее сообщение
 *
 * Class Message
 * @package HoverBot\Base
 */
class Message
{
    /** @var string Канал */
    protected $channel;

    /** @var string Текст */
    protected $text;

    /** @var string Имя отправителя */
    protected $username;

    /** @var boolean Отправить как пользователь */
    protected $as_user = true;

    /** @var string Тип парсинга сообщения */
    protected $parse = 'full';

    /** @var boolean Связывать имена */
    protected $link_names = true;

    /** @var MessageAttachment[] Вложения */
    protected $attachments = [];

    /** @var string Ссылка на картинку */
    protected $icon_url;

    /** @var string Эмодзи картинка */
    protected $icon_emoji;

    /**
     *
     * @param string $text
     */
    public function __construct($text = null)
    {
        $this->setText($text);
    }

    /**
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     *
     * @return Message
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param string $text
     * @param string $delimiter
     *
     * @return Message
     */
    public function appendText ($text, $delimiter = "\n")
    {
        if (! $text)
            $this->setText($text);
        else
            $this->setText($this->getText() . $delimiter . $text);

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return Message
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAsUser()
    {
        return $this->as_user;
    }

    /**
     * @param boolean $as_user
     *
     * @return Message
     */
    public function setAsUser($as_user)
    {
        $this->as_user = $as_user;

        return $this;
    }

    /**
     * @return string
     */
    public function getParse()
    {
        return $this->parse;
    }

    /**
     * @param string $parse
     *
     * @return Message
     */
    public function setParse($parse)
    {
        $this->parse = $parse;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isLinkNames()
    {
        return $this->link_names;
    }

    /**
     * @param boolean $link_names
     *
     * @return Message
     */
    public function setLinkNames($link_names)
    {
        $this->link_names = $link_names;

        return $this;
    }

    /**
     * @return MessageAttachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param MessageAttachment[] $attachments
     *
     * @return Message
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Добавляет новое вложение
     *
     * @param MessageAttachment $attachment
     *
     * @return Message
     */
    public function addAttachment (MessageAttachment &$attachment)
    {
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Очищает вложения

     * @return Message
     */
    public function clearFields ()
    {
        $this->attachments = [];

        return $this;
    }

    /**
     * @return string
     */
    public function getIconUrl()
    {
        return $this->icon_url;
    }

    /**
     * @param string $icon_url
     *
     * @return Message
     */
    public function setIconUrl($icon_url)
    {
        $this->icon_url = $icon_url;

        return $this;
    }

    /**
     * @return string
     */
    public function getIconEmoji()
    {
        return $this->icon_emoji;
    }

    /**
     * @param string $icon_emoji
     *
     * @return Message
     */
    public function setIconEmoji($icon_emoji)
    {
        $this->icon_emoji = $icon_emoji;

        return $this;
    }

    public function toArray ()
    {
        $attachments = [];

        foreach ($this->getAttachments() as $attachment)
            $attachments[] = $attachment->toArray();

        return [
            'channel'       => $this->getChannel(),
            'text'          => $this->getText(),
            'username'      => $this->getUsername(),
            'as_user'       => (int)$this->isAsUser(),
            'parse'         => $this->getParse(),
            'link_names'    => (int)$this->isLinkNames(),
            'icon_url'      => $this->getIconUrl(),
            'icon_emoji'    => $this->getIconEmoji(),
            'attachments'   => json_encode($attachments),
        ];
    }
}