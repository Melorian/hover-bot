<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 06.11.15
 * Time: 15:10
 */

namespace HoverBot\Base;

/**
 * Диалог
 *
 * Class Im
 * @package HoverBot\Base
 */
class Im
{
    /** @var Context Текущий контекст подключения */
    protected $context;

    /** @var string Идентификатор */
    protected $id;

    /** @var boolean Диалог является диалогом ಠ_ಠ */
    protected $is_im;

    /** @var User Собеседник */
    protected $user;

    /** @var integer Дата создания */
    protected $created;

    /** @var boolean Собеседник удален */
    protected $is_user_deleted;

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(Context &$context, array $data)
    {
        $this->context = $context;

        $this->setId(isset($data['id']) ? $data['id'] : null);
        $this->setIsIm(isset($data['is_im']) ? $data['is_im'] : null);
        $this->setUser(isset($data['user']) ? $context->getUser($data['user']) : null);
        $this->setCreated(isset($data['created']) ? $data['created'] : null);
        $this->setIsUserDeleted(isset($data['is_user_deleted']) ? $data['is_user_deleted'] : null);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Im
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsIm()
    {
        return $this->is_im;
    }

    /**
     * @param boolean $is_im
     *
     * @return Im
     */
    public function setIsIm($is_im)
    {
        $this->is_im = $is_im;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Im
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     *
     * @return Im
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsUserDeleted()
    {
        return $this->is_user_deleted;
    }

    /**
     * @param boolean $is_user_deleted
     *
     * @return Im
     */
    public function setIsUserDeleted($is_user_deleted)
    {
        $this->is_user_deleted = $is_user_deleted;

        return $this;
    }

}