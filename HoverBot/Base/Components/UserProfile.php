<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 10:42
 */

namespace HoverBot\Base\Components;

use HoverBot\Base\User;

class UserProfile
{
    /** @var User Пользователь */
    protected $user;

    /** @var string Имя профиля */
    protected $first_name;

    /** @var string Фамилия профиля */
    protected $last_name;

    /** @var string Имя профиля полностью */
    protected $real_name;

    /** @var string Email */
    protected $email;

    /** @var string Скайп */
    protected $skype;

    /** @var string Телефон */
    protected $phone;

    /** @var string Аватар 24х24 */
    protected $image_24;

    /** @var string Аватар 3232 */
    protected $image_32;

    /** @var string Аватар 4848 */
    protected $image_48;

    /** @var string Аватар 72х72 */
    protected $image_72;

    /** @var string Аватар 192192 */
    protected $image_192;

    public function __construct (User &$user, array $data)
    {
        $this->user = $user;

        $this->setFirstName(isset($data['first_name']) ? $data['first_name'] : null);
        $this->setLastName(isset($data['last_name']) ? $data['last_name'] : null);
        $this->setRealName(isset($data['real_name']) ? $data['real_name'] : null);
        $this->setEmail(isset($data['email']) ? $data['email'] : null);
        $this->setSkype(isset($data['skype']) ? $data['skype'] : null);
        $this->setPhone(isset($data['phone']) ? $data['phone'] : null);
        $this->setImage24(isset($data['image_24']) ? $data['image_24'] : null);
        $this->setImage32(isset($data['image_32']) ? $data['image_32'] : null);
        $this->setImage48(isset($data['image_48']) ? $data['image_48'] : null);
        $this->setImage72(isset($data['image_72']) ? $data['image_72'] : null);
        $this->setImage192(isset($data['image_192']) ? $data['image_192'] : null);
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     *
     * @return UserProfile
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     *
     * @return UserProfile
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRealName()
    {
        return $this->real_name;
    }

    /**
     * @param string $real_name
     *
     * @return UserProfile
     */
    public function setRealName($real_name)
    {
        $this->real_name = $real_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return UserProfile
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param string $skype
     *
     * @return UserProfile
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return UserProfile
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage24()
    {
        return $this->image_24;
    }

    /**
     * @param string $image_24
     *
     * @return UserProfile
     */
    public function setImage24($image_24)
    {
        $this->image_24 = $image_24;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage32()
    {
        return $this->image_32;
    }

    /**
     * @param string $image_32
     *
     * @return UserProfile
     */
    public function setImage32($image_32)
    {
        $this->image_32 = $image_32;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage48()
    {
        return $this->image_48;
    }

    /**
     * @param string $image_48
     *
     * @return UserProfile
     */
    public function setImage48($image_48)
    {
        $this->image_48 = $image_48;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage72()
    {
        return $this->image_72;
    }

    /**
     * @param string $image_72
     *
     * @return UserProfile
     */
    public function setImage72($image_72)
    {
        $this->image_72 = $image_72;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage192()
    {
        return $this->image_192;
    }

    /**
     * @param string $image_192
     *
     * @return UserProfile
     */
    public function setImage192($image_192)
    {
        $this->image_192 = $image_192;

        return $this;
    }

}