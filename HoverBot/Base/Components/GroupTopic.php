<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 14:10
 */

namespace HoverBot\Base\Components;

use HoverBot\Base\Group;

/**
 * Тема группы
 *
 * Class GroupTopic
 * @package HoverBot\Base\Components
 */
class GroupTopic
{
    /** @var Group Группа  */
    protected $group;

    /** @var string Название  */
    protected $value;

    /** @var string Создатель  */
    protected $creator;

    /** @var integer Дата установки  */
    protected $last_set;

    /**
     * @param Group $group
     * @param array $data
     */
    public function __construct(Group &$group, array $data)
    {
        $this->channel = $group;

        $this->setCreator(isset($data['created']) ? $data['creator'] : null);
        $this->setValue(isset($data['value']) ? $data['value'] : null);
        $this->setLastSet(isset($data['last_set']) ? $data['last_set'] : null);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return ChannelTopic
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     *
     * @return ChannelTopic
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return int
     */
    public function getLastSet()
    {
        return $this->last_set;
    }

    /**
     * @param int $last_set
     *
     * @return ChannelTopic
     */
    public function setLastSet($last_set)
    {
        $this->last_set = $last_set;

        return $this;
    }
}