<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 14:10
 */

namespace HoverBot\Base\Components;

use HoverBot\Base\Channel;

/**
 * Тема канала
 *
 * Class ChannelTopic
 * @package HoverBot\Base\Components
 */
class ChannelTopic
{
    /** @var Channel Канал  */
    protected $channel;

    /** @var string Название  */
    protected $value;

    /** @var string Создатель  */
    protected $creator;

    /** @var integer Дата установки  */
    protected $last_set;

    /**
     * @param Channel $channel
     * @param array $data
     */
    public function __construct(Channel &$channel, array $data)
    {
        $this->channel = $channel;

        $this->setCreator(isset($data['created']) ? $data['creator'] : null);
        $this->setValue(isset($data['value']) ? $data['value'] : null);
        $this->setLastSet(isset($data['last_set']) ? $data['last_set'] : null);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return ChannelTopic
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     *
     * @return ChannelTopic
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return int
     */
    public function getLastSet()
    {
        return $this->last_set;
    }

    /**
     * @param int $last_set
     *
     * @return ChannelTopic
     */
    public function setLastSet($last_set)
    {
        $this->last_set = $last_set;

        return $this;
    }
}