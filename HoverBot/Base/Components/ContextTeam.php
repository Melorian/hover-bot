<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 11:49
 */

namespace HoverBot\Base\Components;

use HoverBot\Base\Context;

class ContextTeam
{
    /** @var Context Текущий контекст подключения */
    protected $context;

    /** @var string Идентификатор */
    protected $id;

    /** @var string Название команды */
    protected $name;

    /** @var string Почтовый домен */
    protected $email_domain;

    /** @var string Домен сайта */
    protected $domain;

    /** @var integer ХЗ что */
    protected $msg_edit_window_mins;

    /** @var boolean Превышение лимита по месту */
    protected $over_storage_limit;

    /** @var array Настройки */
    protected $prefs;

    /** @var string План команды */
    protected $plan;

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(Context &$context, array $data)
    {
        $this->context = $context;

        $this->setId(isset($data['id']) ? $data['id'] : null);
        $this->setName(isset($data['name']) ? $data['name'] : null);
        $this->setEmailDomain(isset($data['email_domain']) ? $data['email_domain'] : null);
        $this->setDomain(isset($data['domain']) ? $data['domain'] : null);
        $this->setMsgEditWindowMins(isset($data['msg_edit_window_mins']) ? $data['msg_edit_window_mins'] : null);
        $this->setOverStorageLimit(isset($data['over_storage_limit']) ? $data['over_storage_limit'] : null);
        $this->setPrefs(isset($data['prefs']) ? $data['prefs'] : null);
        $this->setPlan(isset($data['plan']) ? $data['plan'] : null);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return ContextTeam
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ContextTeam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmailDomain()
    {
        return $this->email_domain;
    }

    /**
     * @param string $email_domain
     *
     * @return ContextTeam
     */
    public function setEmailDomain($email_domain)
    {
        $this->email_domain = $email_domain;

        return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     *
     * @return ContextTeam
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return int
     */
    public function getMsgEditWindowMins()
    {
        return $this->msg_edit_window_mins;
    }

    /**
     * @param int $msg_edit_window_mins
     *
     * @return ContextTeam
     */
    public function setMsgEditWindowMins($msg_edit_window_mins)
    {
        $this->msg_edit_window_mins = $msg_edit_window_mins;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isOverStorageLimit()
    {
        return $this->over_storage_limit;
    }

    /**
     * @param boolean $over_storage_limit
     *
     * @return ContextTeam
     */
    public function setOverStorageLimit($over_storage_limit)
    {
        $this->over_storage_limit = $over_storage_limit;

        return $this;
    }

    /**
     * @return array
     */
    public function getPrefs()
    {
        return $this->prefs;
    }

    /**
     * @param array $prefs
     *
     * @return ContextTeam
     */
    public function setPrefs($prefs)
    {
        $this->prefs = $prefs;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param string $plan
     *
     * @return ContextTeam
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

}