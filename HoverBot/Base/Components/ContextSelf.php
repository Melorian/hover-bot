<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 11:46
 */

namespace HoverBot\Base\Components;

use HoverBot\Base\Context;

/**
 * Текущий авторизованный пользователь
 *
 * Class ContextSelf
 * @package HoverBot\Base\Components
 */
class ContextSelf
{
    /** @var Context Текущий контекст подключения */
    protected $context;

    /** @var string Идентификатор */
    protected $id;

    /** @var string Системное имя */
    protected $name;

    /** @var array Настройки */
    protected $prefs;

    /** @var integer Дата создания */
    protected $created;

    /** @var string Ручной статус в системе */
    protected $manual_presence;

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(Context &$context, array $data)
    {
        $this->context = $context;

        $this->setId(isset($data['id']) ? $data['id'] : null);
        $this->setName(isset($data['name']) ? $data['name'] : null);
        $this->setPrefs(isset($data['prefs']) ? $data['prefs'] : null);
        $this->setCreated(isset($data['created']) ? $data['created'] : null);
        $this->setManualPresence(isset($data['manual_presence']) ? $data['manual_presence'] : null);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return ContextSelf
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ContextSelf
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getPrefs()
    {
        return $this->prefs;
    }

    /**
     * @param array $prefs
     *
     * @return ContextSelf
     */
    public function setPrefs($prefs)
    {
        $this->prefs = $prefs;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     *
     * @return ContextSelf
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return string
     */
    public function getManualPresence()
    {
        return $this->manual_presence;
    }

    /**
     * @param string $manual_presence
     *
     * @return ContextSelf
     */
    public function setManualPresence($manual_presence)
    {
        $this->manual_presence = $manual_presence;

        return $this;
    }


}