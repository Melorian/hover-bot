<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 18:26
 */

namespace HoverBot\Base\Components;

use HoverBot\Exceptions\DataException;

class MessageAttachment
{
    const COLOR_GOOD    = 'good';
    const COLOR_WARNING = 'warning';
    const COLOR_DANGER  = 'danger';

    const MARKDOWN_IN_PRETEXT   = 'pretext';
    const MARKDOWN_IN_TEXT      = 'text';
    const MARKDOWN_IN_FIELD     = 'fields';

    /** @var string Запасное сообщение */
    protected $fallback;

    /** @var string Цвет рамки */
    protected $color;

    /** @var string Предварительный текст */
    protected $pretext;

    /** @var string Имя автора цитаты */
    protected $author_name;

    /** @var string Ссылка на автора цитаты */
    protected $author_link;

    /** @var string Иконка автора цитаты */
    protected $author_icon;

    /** @var string Заголовок */
    protected $title;

    /** @var string Ссылка заголовка */
    protected $title_link;

    /** @var string Текст */
    protected $text;

    /** @var MessageAttachmentField[] Дополнительные поля */
    protected $fields = [];

    /** @var string Ссылка на картинку */
    protected $image_url;

    /** @var string Ссылка на картинку-превью */
    protected $thumb_url;

    /** @var boolean Включает возможность использования Markdown-разметки */
    protected $mrkdwn = true;

    /** @var string[] Для каких секций включена разметка Markdown-разметки */
    protected $mrkdwn_in = [];

    /**
     * @return string
     */
    public function getFallback()
    {
        return $this->fallback;
    }

    /**
     * @param string $fallback
     *
     * @return MessageAttachment
     */
    public function setFallback($fallback)
    {
        $this->fallback = $fallback;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     *
     * @return MessageAttachment
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string
     */
    public function getPretext()
    {
        return $this->pretext;
    }

    /**
     * @param string $pretext
     *
     * @return MessageAttachment
     */
    public function setPretext($pretext)
    {
        $this->pretext = $pretext;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->author_name;
    }

    /**
     * @param string $author_name
     *
     * @return MessageAttachment
     */
    public function setAuthorName($author_name)
    {
        $this->author_name = $author_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorLink()
    {
        return $this->author_link;
    }

    /**
     * @param string $author_link
     *
     * @return MessageAttachment
     */
    public function setAuthorLink($author_link)
    {
        $this->author_link = $author_link;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorIcon()
    {
        return $this->author_icon;
    }

    /**
     * @param string $author_icon
     *
     * @return MessageAttachment
     */
    public function setAuthorIcon($author_icon)
    {
        $this->author_icon = $author_icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return MessageAttachment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitleLink()
    {
        return $this->title_link;
    }

    /**
     * @param string $title_link
     *
     * @return MessageAttachment
     */
    public function setTitleLink($title_link)
    {
        $this->title_link = $title_link;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return MessageAttachment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param string $text
     * @param string $delimiter
     *
     * @return MessageAttachment
     */
    public function appendText ($text, $delimiter = "\n")
    {
        if (! $text)
            $this->setText($text);
        else
            $this->setText($this->getText() . $delimiter . $text);

        return $this;
    }

    /**
     * @return MessageAttachmentField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param MessageAttachmentField[] $fields
     *
     * @return MessageAttachment
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Добавляет новое дополнительное поле
     *
     * @param MessageAttachmentField $field
     *
     * @return MessageAttachment
     */
    public function addField (MessageAttachmentField &$field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Очищает дополнительные поля

     * @return MessageAttachment
     */
    public function clearFields ()
    {
        $this->fields = [];

        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @param string $image_url
     *
     * @return MessageAttachment
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;

        return $this;
    }

    /**
     * @return string
     */
    public function getThumbUrl()
    {
        return $this->thumb_url;
    }

    /**
     * @param string $thumb_url
     *
     * @return MessageAttachment
     */
    public function setThumbUrl($thumb_url)
    {
        $this->thumb_url = $thumb_url;

        return $this;
    }

    /**
     * Список возможных секций для включения Markdown-разметки
     *
     * @return string[]
     */
    public static function markdownSections ()
    {
        return [static::MARKDOWN_IN_FIELD, static::MARKDOWN_IN_PRETEXT, static::MARKDOWN_IN_TEXT];
    }

    public function isMarkdownEnabled ()
    {
        return $this->mrkdwn == true;
    }

    /**
     * Включает использование Markdown-разметки
     *
     * @return MessageAttachment
     */
    public function enableMarkdown ()
    {
        if (! $this->mrkdwn)
            $this->mrkdwn = true;

        return $this;
    }

    /**
     * Отключает использование Markdown-разметки
     *
     * @return MessageAttachment
     */
    public function disableMarkdown ()
    {
        if ($this->mrkdwn)
            $this->mrkdwn = false;

        return $this;
    }

    /**
     * Включает Markdown-разметку в определенной секции
     *
     * @param string $where
     * @param boolean|true $force
     *
     * @return MessageAttachment
     * @throws DataException
     */
    public function enableMarkdownFor ($where, $force = true)
    {
        if (! in_array($where, static::markdownSections()))
            throw new DataException(vsprintf("Неверно указан атрибут %s для включения markdown-размерки в приложении к сообщению", [$where]));

        if (! $this->mrkdwn)
        {
            if ($force)
                $this->enableMarkdown();
            else
                throw new DataException("Для приложения к сообщению отключена Markdown-разметка");
        }

        if (! in_array($where, $this->mrkdwn_in))
            $this->mrkdwn_in[] = $where;

        return $this;
    }

    /**
     * Отключает Markdown-разметку в определенной секции
     *
     * @param string $where
     *
     * @return MessageAttachment
     * @throws DataException
     */
    public function disableMarkdownFor ($where)
    {
        if (! in_array($where, static::markdownSections()))
            throw new DataException(vsprintf("Неверно указан атрибут %s для отключения markdown-размерки в приложении к сообщению", [$where]));

        if (in_array($where, $this->mrkdwn_in))
            unset($this->mrkdwn_in[array_search($where, $this->mrkdwn_in)]);

        return $this;
    }

    public function toArray ()
    {
        $fields = [];

        foreach ($this->getFields() as $field)
            $fields[] = $field->toArray();

        return [
            'fallback'      => $this->getFallback(),
            'color'         => $this->getColor(),
            'pretext'       => $this->getPretext(),
            'author_name'   => $this->getAuthorName(),
            'author_link'   => $this->getAuthorLink(),
            'author_icon'   => $this->getAuthorIcon(),
            'title'         => $this->getTitle(),
            'title_link'    => $this->getTitleLink(),
            'text'          => $this->getText(),
            'image_url'     => $this->getImageUrl(),
            'thumb_url'     => $this->getThumbUrl(),
            'mrkdwn'        => $this->isMarkdownEnabled(),
            'mrkdwn_in'     => $this->mrkdwn_in,
            'fields'        => $fields,
        ];
    }

}