<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 18:31
 */

namespace HoverBot\Base\Components;

class MessageAttachmentField
{
    const SHORT_DEFAULT = true;

    /** @var string Название */
    protected $title;

    /** @var string Значение */
    protected $value;

    /** @var boolean Короткий параметр */
    protected $short = MessageAttachmentField::SHORT_DEFAULT;

    /**
     * MessageAttachmentField constructor.
     *
     * @param string $title
     * @param string $value
     * @param bool   $short
     */
    public function __construct($title, $value, $short = MessageAttachmentField::SHORT_DEFAULT)
    {
        $this->setTitle($title);
        $this->setValue($value);
        $this->setShort($short);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return MessageAttachmentField
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return MessageAttachmentField
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isShort()
    {
        return $this->short;
    }

    /**
     * @param boolean $short
     *
     * @return MessageAttachmentField
     */
    public function setShort($short)
    {
        $this->short = $short;

        return $this;
    }

    public function toArray ()
    {
        return [
            'title' => $this->getTitle(),
            'value' => $this->getValue(),
            'short' => (int)$this->isShort(),
        ];
    }

}