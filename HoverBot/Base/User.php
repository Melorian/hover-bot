<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 10:37
 */

namespace HoverBot\Base;

use HoverBot\Base\Components\UserProfile;
use HoverBot\Exceptions\DataException;

class User
{
    const TWO_FACTOR_TYPE_SMS   = 'sms';
    const TWO_FACTOR_TYPE_APP   = 'app';

    /** @var Context Текущий контекст подключения */
    protected $context;

    /** @var string Идентификатор */
    protected $id;

    /** @var string Системное имя */
    protected $name;

    /** @var boolean Отключен */
    protected $deleted;

    /** @var string Уникальный цвет имени (RGB) */
    protected $color;

    /** @var UserProfile Объект профиля */
    protected $profile;

    /** @var boolean Является администратором */
    protected $is_admin;

    /** @var boolean Является владельцем */
    protected $is_owner;

    /** @var boolean Является главным владельцем */
    protected $is_primary_owner;

    /** @var boolean Ограничен */
    protected $is_restricted;

    /** @var boolean Полностью ограничен */
    protected $is_ultra_restricted;

    /** @var boolean Имеет двух-факторную верификацию */
    protected $has_2fa;

    /** @var string Тип двух-факторной верификации */
    protected $two_factor_type;

    /** @var boolean Имеет загруженные файлы */
    protected $has_files;


    public function __construct (Context $context, array $data)
    {
        $this->context = $context;

        $this->setId(isset($data['id']) ? $data['id'] : null);
        $this->setName(isset($data['name']) ? $data['name'] : null);
        $this->setDeleted(isset($data['deleted']) ? $data['deleted'] : null);
        $this->setColor(isset($data['color']) ? $data['color'] : null);
        $this->setIsAdmin(isset($data['is_admin']) ? $data['is_admin'] : null);
        $this->setIsOwner(isset($data['is_owner']) ? $data['is_owner'] : null);
        $this->setIsPrimaryOwner(isset($data['is_primary_owner']) ? $data['is_primary_owner'] : null);
        $this->setIsRestricted(isset($data['is_restricted']) ? $data['is_restricted'] : null);
        $this->setIsUltraRestricted(isset($data['is_ultra_restricted']) ? $data['is_ultra_restricted'] : null);
        $this->setHas2fa(isset($data['has_2fa']) ? $data['has_2fa'] : null);
        $this->setTwoFactorType(isset($data['two_factor_type']) ? $data['two_factor_type'] : null);
        $this->setHasFiles(isset($data['has_files']) ? $data['has_files'] : null);

        if (isset($data['profile']))
            $this->setProfile(new UserProfile($this, $data['profile']));
        else
            $this->setProfile(new UserProfile($this, []));
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param boolean $deleted
     *
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = !!$deleted;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     *
     * @return User
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return UserProfile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param UserProfile $profile
     *
     * @return User
     */
    public function setProfile(UserProfile $profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->is_admin;
    }

    /**
     * @param boolean $is_admin
     *
     * @return User
     */
    public function setIsAdmin($is_admin)
    {
        $this->is_admin = !!$is_admin;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsOwner()
    {
        return $this->is_owner;
    }

    /**
     * @param boolean $is_owner
     *
     * @return User
     */
    public function setIsOwner($is_owner)
    {
        $this->is_owner = !!$is_owner;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPrimaryOwner()
    {
        return $this->is_primary_owner;
    }

    /**
     * @param boolean $is_primary_owner
     *
     * @return User
     */
    public function setIsPrimaryOwner($is_primary_owner)
    {
        $this->is_primary_owner = !!$is_primary_owner;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsRestricted()
    {
        return $this->is_restricted;
    }

    /**
     * @param boolean $is_restricted
     *
     * @return User
     */
    public function setIsRestricted($is_restricted)
    {
        $this->is_restricted = !!$is_restricted;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsUltraRestricted()
    {
        return $this->is_ultra_restricted;
    }

    /**
     * @param boolean $is_ultra_restricted
     *
     * @return User
     */
    public function setIsUltraRestricted($is_ultra_restricted)
    {
        $this->is_ultra_restricted = !!$is_ultra_restricted;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getHas2fa()
    {
        return $this->has_2fa;
    }

    /**
     * @param boolean $has_2fa
     *
     * @return User
     */
    public function setHas2fa($has_2fa)
    {
        $this->has_2fa = !!$has_2fa;

        return $this;
    }

    /**
     * @return string
     */
    public function getTwoFactorType()
    {
        return $this->two_factor_type;
    }

    /**
     * @param string $two_factor_type
     *
     * @return User
     * @throws DataException
     */
    public function setTwoFactorType($two_factor_type)
    {
        if ($two_factor_type)
        {
            $two_factor_type = mb_strtolower($two_factor_type);

            if (! in_array($two_factor_type, [static::TWO_FACTOR_TYPE_APP, static::TWO_FACTOR_TYPE_SMS]))
                throw new DataException(vsprintf('Неверно указан тип двух-факторной верификации %s', $two_factor_type));
        }

            $this->two_factor_type = $two_factor_type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getHasFiles()
    {
        return $this->has_files;
    }

    /**
     * @param boolean $has_files
     *
     * @return User
     */
    public function setHasFiles($has_files)
    {
        $this->has_files = !!$has_files;

        return $this;
    }

    public function toChatId ($full = true)
    {
        return '<@' . $this->getId() . ($full ? '|' . $this->getName() : '') . '>';
    }
}