<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 02.11.15
 * Time: 13:34
 */

namespace HoverBot\Base;

use HoverBot\Base\Components\GroupPurpose;
use HoverBot\Base\Components\GroupTopic;
use HoverBot\Exceptions\DataException;
use HoverBot\Exceptions\Exception;

/**
 * Приватная группа
 *
 * Class Group
 * @package HoverBot\Base
 */
class Group
{
    /** @var Context Текущий контекст подключения */
    protected $context;

    /** @var string Идентификатор */
    protected $id;

    /** @var string Название */
    protected $name;

    /** @var boolean Группа является группой ಠ_ಠ */
    protected $is_group = true;

    /** @var integer Дата создания */
    protected $created;

    /** @var User Создатель */
    protected $creator;

    /** @var boolean Группа в архиве */
    protected $is_archived;

    /** @var boolean Группа является мульти-диалогом */
    protected $is_mpim;

    /** @var User[] Участники */
    protected $members = [];

    /** @var GroupTopic Тема группы */
    protected $topic;

    /** @var GroupPurpose Причина создания группы */
    protected $purpose;

    /** @var string Дата последнего прочтения */
    protected $last_read;

    /** @var Event Последнее сообщение в чате */
    protected $latest;

    /** @var integer Количество непрочтенных сообщений всего */
    protected $unread_count;

    /** @var integer Количество непрочтенных сообщений для пользователя */
    protected $unread_count_display;

    /**
     * @param Context $context
     * @param array $data
     * @throws DataException
     */
    public function __construct(Context &$context, array $data)
    {
        $this->context = $context;

        if (isset($data['topic']))
            $this->setTopic(new GroupTopic($this, $data['topic']));
        else
            $this->setTopic(new GroupTopic($this, []));

        if (isset($data['purpose']))
            $this->setPurpose(new GroupPurpose($this, $data['purpose']));
        else
            $this->setPurpose(new GroupPurpose($this, []));

        $this->setId(isset($data['id']) ? $data['id'] : null);
        $this->setName(isset($data['name']) ? $data['name'] : null);
        $this->setIsGroup(isset($data['is_group']) ? $data['is_group'] : null);
        $this->setCreated(isset($data['created']) ? $data['created'] : null);
        $this->setCreator(isset($data['creator']) ? $context->getUser($data['creator']) : null);
        $this->setIsArchived(isset($data['is_archived']) ? $data['is_archived'] : null);
        $this->setIsMpim(isset($data['is_mpim']) ? $data['is_mpim'] : null);
        $this->setLastRead(isset($data['last_read']) ? $data['last_read'] : null);
        $this->setUnreadCount(isset($data['unread_count']) ? $data['unread_count'] : null);
        $this->setUnreadCountDisplay(isset($data['unread_count_display']) ? $data['unread_count_display'] : null);

        try
        {
            $this->setLatest(isset($data['latest']) ? Event::createFromArray($data['latest']) : null);
        }
        catch (Exception $e)
        {
            $context->getBot()->log(vsprintf("\t\tНе удалось получить последнее сообщение в группе %s: %s", [$this->getId(), $e->getMessage()]));
        }

        if (! empty($data['members']))
            foreach ($data['members'] as $member)
                $this->addMember($context->getUser($member));
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Group
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsGroup()
    {
        return $this->is_group;
    }

    /**
     * @param boolean $is_group
     *
     * @return Group
     */
    public function setIsGroup($is_group)
    {
        $this->is_group = $is_group;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param int $created
     *
     * @return Group
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     *
     * @return Group
     */
    public function setCreator(User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsArchived()
    {
        return $this->is_archived;
    }

    /**
     * @param boolean $is_archived
     *
     * @return Group
     */
    public function setIsArchived($is_archived)
    {
        $this->is_archived = $is_archived;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsMpim()
    {
        return $this->is_mpim;
    }

    /**
     * @param boolean $is_mpim
     *
     * @return Group
     */
    public function setIsMpim($is_mpim)
    {
        $this->is_mpim = $is_mpim;

        return $this;
    }

    /**
     * @return User[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param User[] $members
     *
     * @return Group
     */
    public function setMembers($members)
    {
        $this->members = $members;

        return $this;
    }

    /**
     * Добавляет нового члена канала
     *
     * @param User $user
     * @param boolean|true $replaceIfExists Заменить при совпадении
     *
     * @return Group
     * @throws DataException
     */
    public function addMember (User &$user, $replaceIfExists = true)
    {
        if (array_key_exists($user->getId(), $this->members) AND !$replaceIfExists)
            throw new DataException(vsprintf('Пользователь с ID = %s уже добавлен в группу', $user->getId()));

        $this->members[$user->getId()] = $user;

        return $this;
    }

    /**
     * Берет члена канала по ID
     *
     * @param $id
     *
     * @return User
     * @throws DataException
     */
    public function getMember ($id)
    {
        if (! array_key_exists($id, $this->members))
            throw new DataException(vsprintf('Пользователь с ID = %s не является членом группы', $id));

        return $this->members[$id];
    }

    /**
     * @return GroupTopic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param GroupTopic $topic
     *
     * @return Group
     */
    public function setTopic(GroupTopic &$topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * @return GroupPurpose
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @param GroupPurpose $purpose
     *
     * @return Group
     */
    public function setPurpose(GroupPurpose &$purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastRead()
    {
        return $this->last_read;
    }

    /**
     * @param string $last_read
     *
     * @return Group
     */
    public function setLastRead($last_read)
    {
        $this->last_read = $last_read;

        return $this;
    }

    /**
     * @return Event
     */
    public function getLatest()
    {
        return $this->latest;
    }

    /**
     * @param Event $latest
     *
     * @return Group
     */
    public function setLatest($latest)
    {
        $this->latest = $latest;

        return $this;
    }

    /**
     * @return int
     */
    public function getUnreadCount()
    {
        return $this->unread_count;
    }

    /**
     * @param int $unread_count
     *
     * @return Group
     */
    public function setUnreadCount($unread_count)
    {
        $this->unread_count = $unread_count;

        return $this;
    }

    /**
     * @return int
     */
    public function getUnreadCountDisplay()
    {
        return $this->unread_count_display;
    }

    /**
     * @param int $unread_count_display
     *
     * @return Group
     */
    public function setUnreadCountDisplay($unread_count_display)
    {
        $this->unread_count_display = $unread_count_display;

        return $this;
    }



}