<?php

namespace HoverBot\Base;

use HoverBot\Base\Components\MessageAttachment;
use HoverBot\Base\Components\MessageAttachmentField;
use HoverBot\Exceptions\CommandException;
use HoverBot\Exceptions\Exception;

abstract class ControllerCommand extends SimpleCommand
{
    protected $actions = [];

    /**
     * Возвращает возможные действия
     *
     * @return array
     */
    abstract protected function actions ();

    public function initialize ()
    {
        parent::initialize();

        $actions = $this->actions();

        if (! is_array($actions))
            $actions = [];

        $this->actions = $actions;
    }

    public function execute ()
    {
        try
        {
            $request = $this->parseArguments($this->getEvent()->getText());

            $arguments = $request->getArguments();

            switch ($this->commandMode)
            {
                default:
                    throw new CommandException(vsprintf("Неверный тип вызова команды %s", [$this->commandMode]));

                case static::COMMAND_MODE_PLAIN:
                    $commandArgument = 1;
                    break;

                case static::COMMAND_MODE_SLASH:
                    $commandArgument = 1;
                    break;

                case static::COMMAND_MODE_CALL:
                    $commandArgument = 2;
                    break;
            }

            $action = empty($arguments[$commandArgument]) ? null : mb_strtolower($arguments[$commandArgument]);

            if (! $action OR $action == 'help')
                return $this->helpAction();

            if (! array_key_exists($action, $this->actions))
                throw new CommandException(vsprintf("Не существует обработчик действия %s", [$action]));

            $currentAction = $this->actions[$action];

            if (empty($currentAction[0]))
                throw new CommandException(vsprintf("Не указан обработчик действия %s", [$action]));

            $handler = $currentAction[0].'Action';

            if (! method_exists($this, $handler))
                throw new CommandException(vsprintf("Не найден обработчик действия %s", [$action]));

            $params         = empty($currentAction['params']) ? [] : $currentAction['params'];
            $actionParams   = [];

            $i = 1;
            foreach ($params as $param => $description)
            {
                $j              = $i + $commandArgument;
                $actionParams[] = @$arguments[$j] ?: null;

                $i++;
            }

            return call_user_func_array([$this, $handler], $actionParams);
        }
        catch (Exception $e)
        {
            $this->getBot()->log(vsprintf("Контроллер %s не смог обработать событие: %s", [get_called_class(), $e->getMessage()]));
        }
    }

    public function helpAction ()
    {
        $message = new Message(vsprintf("Помощь по команде *%s*", [$this->command]));

        if (count($this->actions))
            $message->appendText("Список доступных действий:");

        foreach ($this->actions as $name => $info)
        {
            $attachment = new MessageAttachment();
            $attachment->setTitle($name);
            $attachment->enableMarkdownFor(MessageAttachment::MARKDOWN_IN_TEXT);

            if (! empty($info['description']))
                $attachment->setText($info['description']);
            else
                $attachment->setText("_Описание отсутствует_");

            if (! empty($info['params']) AND is_array($info['params']))
            {
                $attachment->appendText("Параметры команды:");

                foreach ($info['params'] as $param => $description)
                {
                    $field = new MessageAttachmentField($param, $description);
                    $attachment->addField($field);
                }
            }

            $message->addAttachment($attachment);
        }

        $message->setChannel($this->getEvent()->getChannel());
        $this->getBot()->getApi()->chatPostMessage($message);
    }
}