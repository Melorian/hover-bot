<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 06.11.15
 * Time: 13:16
 */

namespace HoverBot\Commands;

use HoverBot\Base\ControllerCommand;
use HoverBot\Base\SimpleCommand;

class TestControllerCommand extends ControllerCommand
{
    protected function command ()
    {
        return 'test';
    }

    protected function actions ()
    {
        return [
            'write' => ['write'],
        ];
    }

    protected $commandMode = SimpleCommand::COMMAND_MODE_CALL;
}