<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 09.11.15
 * Time: 16:57
 */

namespace HoverBot\Commands;

use HoverBot\Base\Components\MessageAttachment;
use HoverBot\Base\ControllerCommand;
use HoverBot\Base\Message;
use HoverBot\Exceptions\CommandException;
use HoverBot\Exceptions\Exception;

class CurrencyControllerCommand extends ControllerCommand
{
    const URI = 'http://cbr.ru/scripts/XML_daily.asp';

    protected $rates = [];

    public function initialize ()
    {
        parent::initialize();

        $this->updateRates();
    }

    protected function command ()
    {
        return 'currency';
    }

    protected function actions ()
    {
        return [
            'convert' => [
                'convert',
                'description' => 'Конвертирует одну валюту в другую',
                'params' => [
                    'from'  => 'Исходная валюта',
                    'to'    => 'Желаемая валюта',
                    'amount'=> 'Сумма для конвертации',
                ]
            ],

            'rate' => [
                'rate',
                'description' => 'Отображает котировку двух валют',
                'params' => [
                    'from'  => 'Исходная валюта',
                    'to'    => 'Желаемая валюта',
                ]
            ],
        ];
    }

    public function updateRates()
    {
        try
        {
            libxml_use_internal_errors(true);

            $xml_data   = file_get_contents(static::URI);
            $xml        = simplexml_load_string($xml_data);

            if ($xml === false)
                throw new CommandException("Отсутствуют данные о курсах валют");

            $attributes = [];

            foreach($xml->attributes() as $k => $v)
                $attributes[$k]  = (string)$v;

            $rubles = [];

            // Для начала получаем список курсов по отношению к рублю
            foreach ($xml->Valute as $v)
                $rubles[mb_strtoupper((string)$v->CharCode)] = floatval(str_replace(',', '.', $v->Value)) / floatval($v->Nominal);

            // Получаем список валют, для которых есть курс
            $currencies = array_keys($rubles);

            $result = ['RUB' => $rubles];

            while (count($currencies) > 0)
            {
                $current = array_pop($currencies);

                if ($current == 'RUB')
                    continue;

                $firstRate = $rubles[$current];
                $temp = $currencies;

                $result[$current] = [];

                foreach ($temp as $currency)
                {
                    $secondRate = $rubles[$currency];
                    $result[$current][$currency] = floatval($secondRate) / floatval($firstRate);
                }
            }

            $this->rates = $result;

            $this->getBot()->log("Курсы валют успешно обновлены");
        }
        catch (Exception $e)
        {
            $this->getBot()->log(vsprintf("Не удалось обновить курсы валют: %s", [$e->getMessage()]));
        }
    }

    protected function getRate ($from, $to)
    {
        if (!$from OR !$to)
            throw new CommandException("Необходимо указать исходную и желаемую валюты для конвертирования");

        if ($from == $to)
            return 1;

        if (array_key_exists($from, $this->rates) AND array_key_exists($to, $this->rates[$from]))
            return $this->rates[$from][$to];
        elseif (array_key_exists($to, $this->rates) AND array_key_exists($from, $this->rates[$to]))
            return 1 / $this->rates[$to][$from];
        else
            throw new CommandException(vsprintf("Не найдены котировки для конвертирования из `%s` в `%s`", [$from, $to]));
    }

    public function convertAction ($from, $to, $amount = null)
    {
        try
        {
            $from   = mb_strtoupper($from);
            $to     = mb_strtoupper($to);

            $rate = $this->getRate($from, $to);

            if (! $amount)
                $amount = 1;

            $result = $amount / $rate;

            return $this->answer(vsprintf("По текущему курсу `%s %s` в `%s`:", [$amount, $from, $to]), $result);
        }
        catch (Exception $e)
        {
            return $this->answer("Не удалось получить курс:", $e->getMessage(), MessageAttachment::COLOR_DANGER);
        }
    }

    public function rateAction ($from, $to)
    {
        return $this->convertAction($from, $to, 1);
    }

    protected function answer ($text, $attachmentText = null, $statusColor = MessageAttachment::COLOR_GOOD)
    {
        $message = new Message($text);
        $message->setChannel($this->getEvent()->getChannel());

        if ($attachmentText)
        {
            $attachment = new MessageAttachment();
            $attachment->setTitle($attachmentText);
            $attachment->setColor($statusColor);

            $message->addAttachment($attachment);
        }

        return $this->getBot()->getApi()->chatPostMessage($message);
    }
}