<?php

namespace HoverBot;

use HoverBot\Base\Message;
use HoverBot\Base\User;
use HoverBot\Events\MessageEvent;
use HoverBot\Exceptions\ApiException;

class Api
{
    /** @var Bot Бот */
    protected $bot;

    /** @var array Параметры для запросов к API */
    private $params = [];

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return Api
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @param string $param
     * @param string $value
     * @return Api
     */
    public function setParam($param, $value)
    {
        $this->params[(string)$param] = (string)$value;
        return $this;
    }

    /**
     * @param Bot $bot
     */
    public function __construct(Bot $bot)
    {
        $this->setBot($bot);
    }

    /**
     * @return Bot
     */
    public function getBot()
    {
        return $this->bot;
    }

    /**
     * @param Bot $bot
     * @return Api
     */
    public function setBot($bot)
    {
        $this->bot = $bot;
        return $this;
    }

    public function call ($action, array $data = [])
    {
        $data['token'] = $this->getBot()->getToken();

        $url    = 'https://slack.com/api/' . $action;
        $ch     = curl_init();
        $params = array_merge($this->getParams(), $data);

        curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close($ch);

        if (! $response)
            throw new ApiException('Ответ сервера пуст');

        $response = json_decode($response, true);

        if (! $response)
            throw new ApiException('Не удалось обработать ответ сервера');

        if (isset($response['error']))
            throw new ApiException(vsprintf('Сервер вернул ошибку: %s', $response['error']));

        return $response;
    }

    /*******************************************************************************************************************/

    public function rtmStart ()
    {
        $params = [
            'simple_latest' => true,
            'no_unreads'    => true,
        ];

        return $this->call('rtm.start');
    }

    public function chatPostMessage (Message &$message)
    {
        $this->getBot()->log('Отправляем сообщение');
        return $this->call('chat.postMessage', $message->toArray());
    }

    public function chatAnswerMessage (Message &$message, MessageEvent &$event)
    {
        $user = $this->getBot()->getContext()->getUser($event->getUser());
        $channel = $this->getBot()->getContext()->findEntityById($event->getChannel());

        $message->setText($user->toChatId().':'.$message->getText());
        $message->setChannel($channel->getId());

        return $this->chatPostMessage($message);
    }

    public function chatReplaceMessage (Message &$message, MessageEvent &$event)
    {
        $user = $this->getBot()->getContext()->getUser($event->getUser());
        $channel = $this->getBot()->getContext()->findEntityById($event->getChannel());

        $message->setChannel($event->getChannel());
        $message = $message->toArray();
        $message['ts'] = $event->getTs();

        return $this->call('chat.update', $message);
    }

    public function imOpen (User $user)
    {
        $response = $this->call('im.open', ['user' => $user->getId()]);

        if (empty($response['channel']) OR empty($response['channel']['id']))
            throw new ApiException(vsprintf('Не удалось получить идентификатор диалога с пользователем', [$user->getId()]));

        return $response['channel']['id'];
    }

}