<?php
namespace HoverBot;

use HoverBot\Base\Command;
use HoverBot\Base\Context;
use HoverBot\Base\Event;
use HoverBot\Exceptions\ApiException;
use HoverBot\Exceptions\CommandException;
use HoverBot\Exceptions\ConfigurationException;
use HoverBot\Exceptions\DataException;
use \Zend\Log\Logger;
use \Zend\Log\Writer\Stream;
use \React\EventLoop\Factory;
use \Devristo\Phpws\Client\WebSocket;

class Bot
{
    /** @var string Токен бота для Slack */
    private $token;

    /** @var \Zend\Log\Logger логгер событий */
    private $logger;

    /** @var array Текущий контекст подключения к API */
    private $context = [];

    /** @var \React\EventLoop\LoopInterface текущий процесс работы бота */
    private $loop;

    /** @var WebSocket текущий сокет */
    private $socket;

    /** @var Api текущий сокет */
    private $api;

    /** @var \HoverBot\Base\Command[] список доступных комманд */
    private $commands = [];

    protected $initialized = false;

    public function log ($message, $type = null)
    {
        if (is_array($message))
            $message = var_export($message, true);

        $this->getLogger()->notice($message);
        return $this;
    }

    public function __construct ($token = null)
    {
        defined('HOVER_BOT_PATH') or define('HOVER_BOT_PATH', dirname(__FILE__));

        $this->logger = new Logger();
        $writer = new Stream("php://output");
        $this->logger->addWriter($writer);

        if ($token)
            $this->setToken($token);
    }

    /**
     * @param Logger $logger
     *
     * @return Bot
     */
    public function setLogger (Logger $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return Logger
     */
    public function getLogger ()
    {
        return $this->logger;
    }

    /**
     * @param $token
     *
     * @return Bot
     */
    public function setToken ($token)
    {
        $this->token = $token;
        return $this->log(vsprintf('Установлен токен %s', $token));
    }

    /**
     * @return string
     */
    public function getToken ()
    {
        return $this->token;
    }

    /**
     * @return \React\EventLoop\LoopInterface
     */
    public function getLoop ()
    {
        return $this->loop;
    }

    /**
     * @return WebSocket
     */
    public function getSocket ()
    {
        return $this->socket;
    }

    /**
     * @return Context
     */
    public function getContext ()
    {
        return $this->context;
    }

    /**
     * @param Context $context
     */
    protected function setContext (Context &$context)
    {
        $this->context = $context;
    }


    /**
     * @return Api
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param Api $api
     * @return Bot
     */
    protected function setApi($api)
    {
        $this->api = $api;
        return $this;
    }

    public function updateCommands ()
    {
        $commandsPath = HOVER_BOT_PATH . '/Commands/';

        $this->log(vsprintf('Начинаю обновление списка команд по пути %s', $commandsPath));

        if (! $commands = scandir($commandsPath))
        {
            $this->log('Не удалось получить список файлов комманд');
            throw new ConfigurationException('Не удалось получить список файлов комманд');
        }

        foreach ($commands as $commandFile)
        {
            if ($commandFile == '.' OR $commandFile == '..')
                continue;

            $commandName = Command::scanCommandNameFromFile($commandFile);

            if (! $commandName)
            {
                $this->log(vsprintf('Пропущен файл %s - файл не является командой', $commandFile));
                continue;
            }

            $commandClass = '\\HoverBot\\Commands\\' . $commandName . 'Command';

            if (! class_exists($commandClass))
            {
                $this->log(vsprintf('Пропущен файл %s - файл не содержит класса команды', $commandFile));
                continue;
            }

            if (! is_subclass_of($commandClass, Command::class))
            {
                $this->log(vsprintf('Пропущен файл %s - класс файла не является командой', $commandFile));
                continue;
            }

            $this->commands[$commandName] = new $commandClass($this);
        }

        if (! count($this->commands))
            throw new ConfigurationException('Не найдено ни одной команды');

        $this->log(vsprintf('Обновление списка команд завершено, используется %s комманд', (string)count($this->commands)));

        return $this;
    }


    public function run()
    {
        if (! $this->getToken())
            throw new ConfigurationException('Необходимо указать токен для бота. Подробности смотрите тут: https://my.slack.com/services/new/bot');

        $this->updateCommands();

        $this->initApi();

        $this->initConnection();

        $this->initLoop();

        $this->initialized = true;
    }

    private function initApi ()
    {
        $this->setApi(new Api($this));
    }

    private function initLoop ()
    {
        $this->loop     = Factory::create();
        $this->socket   = new WebSocket($this->getContext()->getUrl(), $this->getLoop(), $this->getLogger());

        $bot = $this;

        $this->socket->on("request", function($headers) use ($bot)
        {
            $bot->log("Процесс подключения открыт...");
        });

        $this->socket->on("handshake", function() use ($bot)
        {
            $bot->log("Получено рукопожатие...");
        });

        $this->socket->on("connect", function() use ($bot)
        {
            $bot->log("Подключено!");
        });

        $this->socket->on("message", function($message) use ($bot)
        {
            $data = $message->getData();

            $bot->log(vsprintf("Получено сообщение %s: ", $data));

            try
            {
                $event = Event::createFromJson($data);
                $event->setBot($this);

                $this->applyCommands($event);

                $bot->log('Обработка сообщения успешно закончена');
            }
            catch (ApiException $e)
            {
                $bot->log(vsprintf('Сообщение пропущено: %s', $e->getMessage()));
            }
        });

        $this->socket->open();
        $this->loop->run();
    }

    private function initConnection ()
    {
        $response = $this->getApi()->rtmStart();

        $this->setContext(new Context($this, $response));
    }

    public function applyCommands ($event)
    {
        foreach ($this->commands as $command)
            if ($command->canHandleEvent($event))
            {
                $this->log(vsprintf('Применяем команду %s', [get_class($command)]));

                try
                {
                    $command->process($event);
                }
                catch (CommandException $e)
                {
                    $this->log(vsprintf('Не удалось применить команду %s, команда вернула ошибку: %s', [get_class($command), $e->getMessage()]));
                }
                catch (\Exception $e)
                {
                    $this->log(vsprintf('Не удалось применить команду %s, исключительная ошибка: %s', [get_class($command), $e->getMessage()]));
                }
                finally
                {
                    $this->log(vsprintf('Команда %s выполнена', [get_class($command)]));
                }
            }
    }

    public function parseUser ($userName)
    {
        $userName = mb_strtoupper(trim($userName, '<>@'));

        try {
            return $this->getContext()->getUser($userName);
        } catch (DataException $e) {
            return null;
        }
    }

}
