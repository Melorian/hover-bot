<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 06.11.15
 * Time: 16:31
 */

namespace HoverBot\Events;

use HoverBot\Base\Event;

class ChannelCreatedEvent extends Event
{
    protected $type = 'channel_created';

    protected $channel;

    protected function parseData ()
    {
        $data = $this->raw;

        $this->channel = $this->setChannel(isset($data['channel']) ? $data['channel'] : null);
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     *
     * @return ChannelCreatedEvent
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    protected function callback ()
    {

    }
}