<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 30.10.15
 * Time: 18:17
 */

namespace HoverBot\Events;

use HoverBot\Base\Event;

class MessageEvent extends Event
{
    protected $type = 'message';

    protected $subtype;

    protected $text;

    protected $channel;

    protected $user;

    protected $ts;

    protected $edited;

    protected function parseData ()
    {
        $data = $this->raw;

        $this->setText(isset($data['text']) ? $data['text'] : null);
        $this->setSubtype(isset($data['subtype']) ? $data['subtype'] : null);
        $this->setChannel(isset($data['channel']) ? $data['channel'] : null);
        $this->setUser(isset($data['user']) ? $data['user'] : null);
        $this->setTs(isset($data['ts']) ? $data['ts'] : null);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * @param mixed $subtype
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * @param mixed $ts
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    }

    /**
     * @return mixed
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * @param mixed $edited
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;
    }

    public function getText ()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    protected function setText($text)
    {
        $this->text = (string)$text;
    }
}
