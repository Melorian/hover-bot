<?php
/**
 * Created by PhpStorm.
 * User: lori
 * Date: 30.10.15
 * Time: 16:57
 */
require 'vendor/autoload.php';

if (!function_exists('mb_ucfirst') && function_exists('mb_substr')) {
    function mb_ucfirst($string) {
        $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
        return $string;
    }
}

$bot = new \HoverBot\Bot('xoxb-13559255701-nfcMlCU39vd6Z5qejNIIh62s');
$bot->run();